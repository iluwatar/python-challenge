# http://www.pythonchallenge.com/pc/def/linkedlist.php

import requests

def main():
    # the initial url to open
    start_url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=12345'
    # we follow the links up to maximum times
    open_url(start_url, 0, 400)
    return 0

def open_url(url, counter, max_counter):
    '''
    Recursively parses and follows links from HTTP response.
    :param url: URL to open
    :param counter: count of URLs opened already
    :param max_counter: the maximum how many URLs can be opened
    :return:
    '''

    # check for termination
    if counter >= max_counter:
        return

    # base url
    nothing_base_url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing='

    # make the request and capture the response
    print('{0}: {1}'.format(counter+1, url))
    r = requests.get(url)
    print(r.text)

    # handle the response and make the recursive call
    nothing_str = 'and the next nothing is '
    divide_str = 'Divide by two and keep going'
    html_str = '.html'
    if r.text.find(nothing_str) != -1:
        # generate the next link and follow it
        idx = r.text.find(nothing_str)
        nothing = int(r.text[idx+len(nothing_str):])
        open_url(nothing_base_url+str(nothing), counter+1, max_counter)
    elif r.text.find(divide_str) != -1:
        # generate the next link and follow it
        idx = url.find(nothing_base_url)
        nothing = int(url[idx+len(nothing_base_url):])
        open_url(nothing_base_url+str(nothing/2), counter+1, max_counter)
    elif r.text.find(html_str) != -1:
        # we found the solution, print it and exit
        print('http://www.pythonchallenge.com/pc/def/' + r.text)
        return
    else:
        raise StopIteration('Cannot handle the response.')

if __name__ == "__main__":
    main()
