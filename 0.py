# http://www.pythonchallenge.com/pc/def/0.html

import sys
import math

def main():
    # the current problem's url
    url = 'http://www.pythonchallenge.com/pc/def/0.html'
    # calculate 2 pow 38
    number = str(pow(2, 38))
    # form the next problem's from the calculated number
    print(url.replace('0', number))
    return 0

if __name__ == "__main__":
    main()
