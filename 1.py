# http://www.pythonchallenge.com/pc/def/map.html

import string

def main():

    # translate the input data via character mapping
    encoded_message = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
    in_table = "abcdefghijklmnopqrstuvwxyz"
    out_table = "cdefghijklmnopqrstuvwxyzab"
    translate_table = str.maketrans(in_table, out_table)
    print(encoded_message.translate(translate_table))

    # apply the the translate table to the url
    url_data = 'map'
    print('http://www.pythonchallenge.com/pc/def/' + url_data.translate(translate_table) + '.html')

    return 0

if __name__ == "__main__":
    main()
